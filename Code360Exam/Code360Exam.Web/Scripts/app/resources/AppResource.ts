﻿module Code360.Resources {

    export class AppResource {
        public apiResource: restangular.IElement;

        constructor(private restangular: restangular.IElement) {
            this.apiResource = restangular.all('fe');
        }

        // this is a sample implementation of how i post the data
        // to the api url provided. however, since the link is not working,
        // i decided to not use this.
        // I used a function instead that generates random responses (see AppService.ts)
        public submit(data: Code360.Models.FormViewModel): Code360.Models.ResponseModel {
            this.apiResource.post(data).then((response: Code360.Models.ResponseModel) => {
                return response;
            });

            return null;
        }

    }

    angular.module('code360')
        .service('AppResource', ['Restangular', AppResource]);
}    