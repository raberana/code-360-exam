var Code360;
(function (Code360) {
    var Resources;
    (function (Resources) {
        var AppResource = (function () {
            function AppResource(restangular) {
                this.restangular = restangular;
                this.apiResource = restangular.all('fe');
            }
            // this is a sample implementation of how i post the data
            // to the api url provided. however, since the link is not working,
            // i decided to not use this.
            // I used a function instead that generates random responses (see AppService.ts)
            AppResource.prototype.submit = function (data) {
                this.apiResource.post(data).then(function (response) {
                    return response;
                });
                return null;
            };
            return AppResource;
        })();
        Resources.AppResource = AppResource;
        angular.module('code360').service('AppResource', ['Restangular', AppResource]);
    })(Resources = Code360.Resources || (Code360.Resources = {}));
})(Code360 || (Code360 = {}));
//# sourceMappingURL=AppResource.js.map