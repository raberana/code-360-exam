var Code360;
(function (Code360) {
    var Services;
    (function (Services) {
        var AppService = (function () {
            function AppService() {
                this.score = 0;
                this.form = new Code360.Models.FormViewModel();
                this.skyColorOptions = [
                    'Blue',
                    'Green',
                    'Red',
                    'Orange',
                    'Purple',
                    'Brown'
                ];
                this.playerOptions = [
                    'LeBron James',
                    'Michael Clarke',
                    'Kobe Bryant',
                    'Julius Irving',
                    'Nick Young',
                    'Paul Pierce'
                ];
            }
            Object.defineProperty(AppService.prototype, "resultScore", {
                get: function () {
                    return this.score;
                },
                set: function (resultScore) {
                    this.score = resultScore;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AppService.prototype, "skyColors", {
                get: function () {
                    return this.skyColorOptions;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AppService.prototype, "players", {
                get: function () {
                    return this.playerOptions;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(AppService.prototype, "filleUpForm", {
                get: function () {
                    return this.form;
                },
                set: function (formViewModel) {
                    this.form = formViewModel;
                },
                enumerable: true,
                configurable: true
            });
            // This function simulates a server returning a proper response.
            // The response is strongly-typed to Code360.Models.ResponseModel (ResponseModel.ts)
            // so that we can ensure that the json properties returned are correct.
            AppService.prototype.generateRandomResponse = function (data) {
                var isSuccessful = Math.floor((Math.random() * 10) + 1) > 4;
                var score = isSuccessful ? Math.floor((Math.random() * 100) + 1) : 0;
                var message = isSuccessful ? "Thank you for your submission!" : "Server too busy! Please try again later.";
                return new Code360.Models.ResponseModel(isSuccessful, message, score);
            };
            return AppService;
        })();
        Services.AppService = AppService;
        angular.module('code360').service('AppService', ['AppResource', AppService]);
    })(Services = Code360.Services || (Code360.Services = {}));
})(Code360 || (Code360 = {}));
//# sourceMappingURL=AppService.js.map