﻿module Code360.Services {

    export class AppService {
        private skyColorOptions: string[];
        private playerOptions: string[];
        private score: number = 0;
        private response: Code360.Models.ResponseModel;
        private form: Code360.Models.FormViewModel;

        constructor() {
            this.form = new Code360.Models.FormViewModel();

            this.skyColorOptions = [
                'Blue',
                'Green',
                'Red',
                'Orange',
                'Purple',
                'Brown'
            ];

            this.playerOptions = [
                'LeBron James',
                'Michael Clarke',
                'Kobe Bryant',
                'Julius Irving',
                'Nick Young',
                'Paul Pierce'
            ];

        }

        get resultScore(): number {
            return this.score;
        }

        set resultScore(resultScore: number) {
            this.score = resultScore;
        }

        get skyColors(): string[] {
            return this.skyColorOptions;
        }

        get players(): string[] {
            return this.playerOptions;
        }

        set filleUpForm(formViewModel: Code360.Models.FormViewModel) {
            this.form = formViewModel;
        }

        get filleUpForm(): Code360.Models.FormViewModel {
            return this.form;
        }

        // This function simulates a server returning a proper response.
        // The response is strongly-typed to Code360.Models.ResponseModel (ResponseModel.ts)
        // so that we can ensure that the json properties returned are correct.
        public generateRandomResponse(data: Code360.Models.FormViewModel): Code360.Models.ResponseModel {

            var isSuccessful = Math.floor((Math.random() * 10) + 1) > 4;
            var score = isSuccessful
                ? Math.floor((Math.random() * 100) + 1)
                : 0;
            var message = isSuccessful
                ? "Thank you for your submission!"
                : "Server too busy! Please try again later.";

            return new Code360.Models.ResponseModel(isSuccessful, message, score);
        }

    }

    angular.module('code360')
        .service('AppService', ['AppResource', AppService]);
}   