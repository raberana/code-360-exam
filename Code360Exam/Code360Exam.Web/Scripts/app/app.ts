﻿module Code360 {

    var app = angular.module('code360', [
        'ngResource',
        'ngRoute',
        'ngAnimate',
        'restangular',
        'ngSanitize',
        'ui.select',
        'easypiechart',
        'ui.bootstrap.datetimepicker'
    ]);

    // configure routing
    app.config(['$routeProvider', (routeProvider: ng.route.IRouteProvider) => {
        routeProvider
            .when('/home', {
                templateUrl: '/PartialViews/Home.html',
                controller: 'HomeController',
                controllerAs: 'homeCtrl'
            })
            .when('/result', {
                templateUrl: '/PartialViews/Result.html',
                controller: 'ResultController',
                controllerAs: 'resultCtrl'
            }).
            otherwise({
                redirectTo: '/home'
            });
    }])

    // configure restangular 
        .config([
            'RestangularProvider', (restangularProvider: restangular.IProvider) => {
                restangularProvider.setBaseUrl('http://www.code360.com.au/assessment');
                restangularProvider.setDefaultHeaders({ 'Content-Type': 'application/json' });
            }
        ]);

    app.directive('multiselectvalidator', () => {
        return {
            require: 'ngModel',
            link: (scope, elm, attrs, ctrl) => {
                ctrl.$validators.multiselectvalidator = (modelValue, viewValue) => {
                    angular.element(elm).addClass('field-error');

                    if (ctrl.$isEmpty(modelValue)) {
                        // empty models are not valid
                        return false;
                    }

                    if (modelValue.length == 2) {
                        angular.element(elm).removeClass('field-error');
                        ctrl.$setValidity('players', true);
                        return true;
                    }

                    ctrl.$setValidity('players', false);
                    return modelValue;
                };
            }
        };
    })
        .directive('selectvalidator', () => {
            return {
                require: 'ngModel',
                link: (scope, elm, attrs, ctrl) => {
                    ctrl.$validators.selectvalidator = (modelValue, viewValue) => {
                        angular.element(elm[0].children[0]).css({ "border-color": "#EA3E23", "border-width": "1px", "box-shadow": "inset 0 2px 2px rgba(0, 0, 0, 0.075)" });

                        if (ctrl.$isEmpty(modelValue)) {
                            // empty models are not valid
                            return false;
                        }

                        if (modelValue == 'Blue') {
                            angular.element(elm[0].children[0]).css({ "border-color": "#cccccc", "border-width": "1px" });
                            ctrl.$setValidity('skyColor', true);
                            return true;
                        }

                        ctrl.$setValidity('skyColor', false);
                        return modelValue;
                    };
                }
            };
        })
        .directive('dozenvalidator', () => {
            return {
                require: 'ngModel',
                link: (scope, elm, attrs, ctrl) => {

                    ctrl.$validators.dozenvalidator = (modelValue, viewValue) => {
                        angular.element(elm).addClass('field-error');

                        if (ctrl.$isEmpty(modelValue)) {
                            // empty models are not valid
                            return false;
                        }

                        if (modelValue == '12') {
                            angular.element(elm).removeClass('field-error');
                            ctrl.$setValidity('dozen', true);
                            return true;
                        }

                        ctrl.$setValidity('dozen', false);
                        return modelValue;
                    };
                }
            };
        })
        .directive('namevalidator', () => {
            return {
                require: 'ngModel',
                link: (scope, elm, attrs, ctrl) => {

                    ctrl.$validators.namevalidator = (modelValue, viewValue) => {
                        angular.element(elm).addClass('field-error');

                        if (ctrl.$isEmpty(modelValue)) {
                            // empty models are not valid
                            return false;
                        }

                        if (modelValue) {
                            angular.element(elm).removeClass('field-error');
                            ctrl.$setValidity('dozen', true);
                            return true;
                        }

                        ctrl.$setValidity('dozen', false);
                        return modelValue;
                    };
                }
            };
        })
        .directive('datetimevalidator', () => {
            return {
                require: 'ngModel',
                link: (scope, elm, attrs, ctrl) => {
                    ctrl.$validators.datetimevalidator = (modelValue: any, viewValue) => {
                        angular.element(elm).addClass('field-error');
                        var date = new Date(modelValue).toDateString() + ' ' + new Date(modelValue).toLocaleTimeString();
                        if (ctrl.$isEmpty(modelValue)) {
                            // empty models are not valid

                            return false;
                        }

                        if (new Date(modelValue).getTime() == new Date(2015, 4, 21, 15, 30, 0, 0).getTime()) {
                            angular.element(elm).removeClass('field-error');
                            angular.element(elm).val(date);
                            ctrl.$setValidity('datetime', true);
                            return true;
                        }

                        ctrl.$setValidity('datetime', false);
                        angular.element(elm).val(date);
                        return modelValue;
                    };
                }
            };
        });
} 