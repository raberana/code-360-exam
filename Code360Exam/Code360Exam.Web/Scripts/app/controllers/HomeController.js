var Code360;
(function (Code360) {
    var Controllers;
    (function (Controllers) {
        var HomeController = (function () {
            function HomeController(scope, appService, locationService, anchorScroll) {
                this.scope = scope;
                this.appService = appService;
                this.locationService = locationService;
                this.anchorScroll = anchorScroll;
                this.btnDisable = false;
                this.errorMessage = '';
                // populate player options
                this.players = this.getPlayerOptions();
                // populate sky color options
                this.skyColors = this.getSkyColorOptions();
                // initialize values
                this.errorShow = false;
                this.errorMessage = '';
                this.formViewModel = new Code360.Models.FormViewModel();
                this.btnLabel = "Submit";
                // if user previously submitted a form, 
                // used its details instead
                if (this.appService.filleUpForm)
                    this.formViewModel = this.appService.filleUpForm;
            }
            HomeController.prototype.getSkyColorOptions = function () {
                return this.appService.skyColors;
            };
            HomeController.prototype.getPlayerOptions = function () {
                return this.appService.players;
            };
            HomeController.prototype.submit = function () {
                // tell the parent controller that the next transition to be used is slide down
                this.scope.$emit('setTransition', 'app-slidedown');
                this.btnLabel = "Submitting";
                this.btnDisable = true;
                // Simulate a server request and response
                // http://www.code360.com.au/assessment/fe is not really returning a json object
                // this.appService.generateRandomResponse() will act as server response generator
                var response = this.appService.generateRandomResponse(this.formViewModel);
                this.errorShow = !response.success;
                this.errorMessage = response.message;
                this.appService.resultScore = response.score;
                if (response.success) {
                    // save the submitted form so user can go back to it later
                    this.appService.filleUpForm = this.formViewModel;
                    // go to results page
                    this.locationService.path('/result');
                }
                else {
                    // go to bottom of the page
                    this.locationService.hash('bottom');
                    this.anchorScroll();
                }
                this.btnDisable = false;
                this.btnLabel = "Submit";
            };
            return HomeController;
        })();
        Controllers.HomeController = HomeController;
        angular.module('code360').controller('HomeController', ['$scope', 'AppService', '$location', '$anchorScroll', HomeController]);
    })(Controllers = Code360.Controllers || (Code360.Controllers = {}));
})(Code360 || (Code360 = {}));
//# sourceMappingURL=HomeController.js.map