﻿module Code360.Controllers {

    export class MainController {

        // transitionEffect is the active transition
        // its default value is slideup
        public transitionEffect: string = "app-slideup";

        constructor(private scope: ng.IScope) {

            // actively listen for the events from children controllers
            // that will tell which transition will be used: slideup or slidedown
            this.scope.$on('setTransition', function (event, data) {
                this.transitionEffect = data;
            }.bind(this));
        }
    }

    angular.module('code360')
        .controller('MainController', ['$scope', MainController]);
}    