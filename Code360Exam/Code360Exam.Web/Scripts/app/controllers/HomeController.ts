﻿module Code360.Controllers {

    export class HomeController {
        public formViewModel: Code360.Models.FormViewModel;
        public btnLabel: string;
        public btnDisable: boolean = false;
        public players: string[];
        public skyColors: string[];
        public errorShow: boolean;
        public errorMessage: string = '';

        constructor(private scope: ng.IScope,
            private appService: Code360.Services.AppService,
            private locationService: ng.ILocationService,
            private anchorScroll: ng.IAnchorScrollService) {

            // populate player options
            this.players = this.getPlayerOptions();

            // populate sky color options
            this.skyColors = this.getSkyColorOptions();

            // initialize values
            this.errorShow = false;
            this.errorMessage = '';
            this.formViewModel = new Code360.Models.FormViewModel();
            this.btnLabel = "Submit";

            // if user previously submitted a form, 
            // used its details instead
            if (this.appService.filleUpForm)
                this.formViewModel = this.appService.filleUpForm;
        }

        private getSkyColorOptions(): string[] {
            return this.appService.skyColors;
        }

        private getPlayerOptions(): any[] {
            return this.appService.players;
        }

        public submit() {
            // tell the parent controller that the next transition to be used is slide down
            this.scope.$emit('setTransition', 'app-slidedown');

            this.btnLabel = "Submitting";
            this.btnDisable = true;

            // Simulate a server request and response
            // http://www.code360.com.au/assessment/fe is not really returning a json object
            // this.appService.generateRandomResponse() will act as server response generator
            var response = this.appService.generateRandomResponse(this.formViewModel);

            this.errorShow = !response.success;
            this.errorMessage = response.message;
            this.appService.resultScore = response.score;

            if (response.success) {
                // save the submitted form so user can go back to it later
                this.appService.filleUpForm = this.formViewModel;

                // go to results page
                this.locationService.path('/result');
            } else {
                // go to bottom of the page
                this.locationService.hash('bottom');
                this.anchorScroll();
            }

            this.btnDisable = false;
            this.btnLabel = "Submit";
        }
    }

    angular.module('code360')
        .controller('HomeController', ['$scope', 'AppService', '$location', '$anchorScroll', HomeController]);
}   