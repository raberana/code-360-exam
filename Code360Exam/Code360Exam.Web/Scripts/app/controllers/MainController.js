var Code360;
(function (Code360) {
    var Controllers;
    (function (Controllers) {
        var MainController = (function () {
            function MainController(scope) {
                this.scope = scope;
                // transitionEffect is the active transition
                // its default value is slideup
                this.transitionEffect = "app-slideup";
                // actively listen for the events from children controllers
                // that will tell which transition will be used: slideup or slidedown
                this.scope.$on('setTransition', function (event, data) {
                    this.transitionEffect = data;
                }.bind(this));
            }
            return MainController;
        })();
        Controllers.MainController = MainController;
        angular.module('code360').controller('MainController', ['$scope', MainController]);
    })(Controllers = Code360.Controllers || (Code360.Controllers = {}));
})(Code360 || (Code360 = {}));
//# sourceMappingURL=MainController.js.map