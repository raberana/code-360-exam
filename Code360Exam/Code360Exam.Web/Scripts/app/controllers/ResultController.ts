﻿module Code360.Controllers {

    export class ResultController {
        private redColor: string = '#FF0000';
        private yellowColor: string = '#F7CD19';
        private greenColor: string = '#7AC943';

        public percent: number;
        public scorePercentOpts: any;

        constructor(private scope: ng.IScope,
            private locationService: ng.ILocationService,
            private appService: Code360.Services.AppService) {

            this.initializeResult(this.appService.resultScore);
        }

        private initializeResult(score: number) {
            var color: string = this.greenColor;
            if (score < 50)
                color = this.redColor;
            else if (score < 75)
                color = this.yellowColor;

            this.percent = score;
            this.scorePercentOpts = {
                animate: {
                    duration: 2000,
                    enabled: true
                },
                size: 150,
                barColor: color,
                scaleColor: false,
                lineWidth: 23,
                lineCap: 'circle'
            };
        }

        public resubmit() {
            // tell the parent controller that the next transition to be used is slide up
            this.scope.$emit('setTransition', 'app-slideup');

            // go back to form submission page
            this.locationService.path('/home');
        }

    }

    angular.module('code360')
        .controller('ResultController', ['$scope', '$location', 'AppService', ResultController]);
}   