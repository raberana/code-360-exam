var Code360;
(function (Code360) {
    var Controllers;
    (function (Controllers) {
        var ResultController = (function () {
            function ResultController(scope, locationService, appService) {
                this.scope = scope;
                this.locationService = locationService;
                this.appService = appService;
                this.redColor = '#FF0000';
                this.yellowColor = '#F7CD19';
                this.greenColor = '#7AC943';
                this.initializeResult(this.appService.resultScore);
            }
            ResultController.prototype.initializeResult = function (score) {
                var color = this.greenColor;
                if (score < 50)
                    color = this.redColor;
                else if (score < 75)
                    color = this.yellowColor;
                this.percent = score;
                this.scorePercentOpts = {
                    animate: {
                        duration: 2000,
                        enabled: true
                    },
                    size: 150,
                    barColor: color,
                    scaleColor: false,
                    lineWidth: 23,
                    lineCap: 'circle'
                };
            };
            ResultController.prototype.resubmit = function () {
                // tell the parent controller that the next transition to be used is slide up
                this.scope.$emit('setTransition', 'app-slideup');
                // go back to form submission page
                this.locationService.path('/home');
            };
            return ResultController;
        })();
        Controllers.ResultController = ResultController;
        angular.module('code360').controller('ResultController', ['$scope', '$location', 'AppService', ResultController]);
    })(Controllers = Code360.Controllers || (Code360.Controllers = {}));
})(Code360 || (Code360 = {}));
//# sourceMappingURL=ResultController.js.map