﻿module Code360.Models {
    export class FormViewModel {

        public nameField: string = '';

        public dozenField: number = 0;

        public skyColorField: string = '';

        public dateTimeField: string = '';

        public playerField: string[] = [];

        public sayingField: string = '';

        constructor() { }
    }
} 