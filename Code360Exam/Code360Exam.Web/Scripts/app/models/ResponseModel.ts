﻿module Code360.Models {
    export class ResponseModel {

        public success: boolean;

        public message: string = '';

        public score: number = 0;

        constructor(isSuccessful: boolean,
            responseMessage: string,
            responseScore: number) {
            this.success = isSuccessful;
            this.message = responseMessage;
            this.score = responseScore;
        }
    }
}  