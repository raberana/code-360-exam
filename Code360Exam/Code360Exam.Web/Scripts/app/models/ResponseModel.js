var Code360;
(function (Code360) {
    var Models;
    (function (Models) {
        var ResponseModel = (function () {
            function ResponseModel(isSuccessful, responseMessage, responseScore) {
                this.message = '';
                this.score = 0;
                this.success = isSuccessful;
                this.message = responseMessage;
                this.score = responseScore;
            }
            return ResponseModel;
        })();
        Models.ResponseModel = ResponseModel;
    })(Models = Code360.Models || (Code360.Models = {}));
})(Code360 || (Code360 = {}));
//# sourceMappingURL=ResponseModel.js.map