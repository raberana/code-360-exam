var Code360;
(function (Code360) {
    var Models;
    (function (Models) {
        var FormViewModel = (function () {
            function FormViewModel() {
                this.nameField = '';
                this.dozenField = 0;
                this.skyColorField = '';
                this.dateTimeField = '';
                this.playerField = [];
                this.sayingField = '';
            }
            return FormViewModel;
        })();
        Models.FormViewModel = FormViewModel;
    })(Models = Code360.Models || (Code360.Models = {}));
})(Code360 || (Code360 = {}));
//# sourceMappingURL=FormViewModel.js.map