﻿using System.Web.Mvc;

namespace Code360Exam.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Code 360 Frontend Assessment";
            return View();
        }
    }
}
