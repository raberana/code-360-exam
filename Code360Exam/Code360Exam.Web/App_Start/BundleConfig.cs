﻿using System.Web.Optimization;

namespace Code360Exam.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterStyleSheets(bundles);
            RegisterJavascripts(bundles);
        }

        public static void RegisterJavascripts(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/angular.js",
                "~/Scripts/angular-route.js",
                "~/Scripts/angular-resource.js",
                "~/Scripts/angular.easypiechart.js",
                "~/Scripts/angular-sanitize.js",
                "~/Scripts/angular-animate.js"));

            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
               "~/Scripts/moment.js",
               "~/Scripts/datetimepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-select").Include(
             "~/Scripts/angular-select.js"));

            bundles.Add(new ScriptBundle("~/bundles/restangular").Include(
               "~/Scripts/lodash.js",
               "~/Scripts/restangular.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/app/app.js",
                "~/Scripts/app/models/FormViewModel.js",
                "~/Scripts/app/models/ResponseModel.js",
                "~/Scripts/app/resources/AppResource.js",
                "~/Scripts/app/services/AppService.js",
                "~/Scripts/app/controllers/MainController.js",
                "~/Scripts/app/controllers/HomeController.js",
                "~/Scripts/app/controllers/ResultController.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
              "~/Scripts/jquery-{version}.js",
              "~/Scripts/bootstrap.js"));
        }

        public static void RegisterStyleSheets(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/reset.css",
                 "~/Content/bootstrap.css",
                 "~/Content/font-awesome.css",
                 "~/Content/angular-select.css",
                 "~/Content/angular-select2.css",
                 "~/Content/angular-select2-bootstrap.css",
                 "~/Content/datetimepicker.css",
                 "~/Content/piechart.css",
                 "~/Content/site.css"));
        }
    }
}
