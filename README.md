To create the functional version of the mock-up form, the following third-party framework and libraries were used:

* AngularJS
* Bootstrap
* Angular UI-Select
* Angular EasyPieChart
* Typescript (TS)
* Restangular

The application's JS files were compiled from the TypeScript files which were manually coded.

The font Bebas-Neue seems to be missing in the repository of Google fonts; thus, Open Sans was used instead.

The API link is not working and a function that generates the same responses was used instead.
But, even though, the API link was not actually used, it was shown in the code how it could be utilized (see ApiResource.ts). Restangular was supposed to be used since it offers great handling of Restful API resources.

Directives, found in app.ts/js, were created to validate the inputs into the form.
A resource file (AppResource.ts/js) was used to handle the API POST functionality through Restangular. 
The model entities (FormViewModel.ts/js etc..) are used to strongly-typed the objects, like the form view model and API response, used in the creation of the TypeScript files.


